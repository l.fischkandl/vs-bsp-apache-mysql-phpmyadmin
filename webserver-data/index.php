<html>
    <p>Vorhandene Datenbanken:</p>
    <ul>
    <?php

        $dbhost = "db"; 
        $dbuser = "root"; 
        $dbpass = "test";

        $sql = "SHOW DATABASES";
        
        $link = mysqli_connect($dbhost,$dbuser,$dbpass) or die ('Error connecting to mysql: ' . mysqli_error($link).'\r\n');
        
        if (!($result=mysqli_query($link,$sql))) {
                printf("Error: %s\n", mysqli_error($link));
            }
        
        while( $row = mysqli_fetch_row( $result ) ){
                if (($row[0]!="information_schema") && ($row[0]!="mysql")) {
                    echo "<li>".$row[0]."</li>";
                }
            }
    ?>
    </ul>
</html>
