# Docker Compose Beispiel: Apache/MySQL/phpMyAdmin

## Start
```
docker-compose up 
``` 

## Zugriff
- Apache-Webserver: 
    - [http://localhost:8080/](http://localhost:8080/)
    - Die `index.php` listet alle vorhandenen Datenbanken des Datenbankserver `mysql` auf.
- phpMyAdmin: 
    - [http://localhost:8081/](http://localhost:8081/)

## Beenden 
```
docker-compose down
``` 
